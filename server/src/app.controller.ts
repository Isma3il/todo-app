import { Body, Controller, Get, Param, Post, Res, ValidationPipe } from "@nestjs/common";
import { Pusher } from "../config/firebase.config";
import { FirebaseNotifyDto } from "./dto/firebase.notify.dto";
import { sendMail } from "../config/email.config";


@Controller()
export class AppController {

  @Get(":imgpath")
  seeUploadedFile(@Param("imgpath") image, @Res() res) {
    return res.sendFile(image, { root: "./uploads" });
  }


  @Post("test-fcm")
  setFcmNotification(@Body(ValidationPipe) firebaseNotifyDto:FirebaseNotifyDto) {
    return Pusher(firebaseNotifyDto.fcmToken, firebaseNotifyDto.title, firebaseNotifyDto.body,firebaseNotifyDto.data);
  }

  @Post('mailing')
  sendEmail(){
    return sendMail();
  }


}
