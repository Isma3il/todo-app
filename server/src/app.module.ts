import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { AuthModule } from "./auth/auth.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { typeOrmConfig } from "../config/typeorm.config";
import { TasksModule } from "./tasks/tasks.module";
import { MulterModule } from '@nestjs/platform-express';
import { editFileName, imageFileFilter } from "../utils/file-upload.utils";
import { diskStorage } from "multer";
import { molterConfig } from "../config/molterConfig";
import { AppController } from "./app.controller";
import { NestApplication } from "@nestjs/core";
import { LoggerMiddleware } from "../config/LoggerMiddleware";
import { AuthController } from "./auth/auth.controller";



@Module({
  imports: [TypeOrmModule.forRoot(typeOrmConfig),
    MulterModule.register(molterConfig),
    AuthModule,
    TasksModule],
  controllers: [AppController]

})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(LoggerMiddleware).forRoutes(AuthController)
  }
}
