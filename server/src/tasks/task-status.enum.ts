
export enum TaskStatusEnum {
  OPEN ='OPEN',
  ONGOING='ONGOING',
  DONE = 'DONE'
}
