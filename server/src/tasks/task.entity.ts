import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { TaskStatusEnum } from "./task-status.enum";
import { User } from "../auth/user.entity";


@Entity()
export class Task extends BaseEntity{

  @PrimaryGeneratedColumn()
  task_id:number;

  @Column()
  title:string;

  @Column()
  description:string;

  @Column()
  status:TaskStatusEnum=TaskStatusEnum.OPEN;

  @ManyToOne(type => User , user => user.tasks , {eager:false})
  user:User;

  @Column()
  userId:string;

}
