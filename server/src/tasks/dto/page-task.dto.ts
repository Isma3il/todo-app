import { IsNumber, IsOptional } from "class-validator";

export class PageTaskDto {
  @IsOptional()
  page:number=1;

  @IsOptional()
  limit:number=5;
}
