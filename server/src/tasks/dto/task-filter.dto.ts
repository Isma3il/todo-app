import { TaskStatusEnum } from "../task-status.enum";
import { Optional } from "@nestjs/common";
import { IsEnum, IsNotEmpty } from "class-validator";

export class TaskFilterDto {
  @Optional()
  search:string;

  @Optional()
  status:TaskStatusEnum;
}
