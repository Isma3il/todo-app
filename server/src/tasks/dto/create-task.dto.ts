import { TaskStatusEnum } from "../task-status.enum";
import { IsEnum, IsNotEmpty } from "class-validator";

export class CreateTaskDto {
  @IsNotEmpty()
  title:string;

  @IsNotEmpty()
  description:string;

  @IsEnum(TaskStatusEnum)
  status:TaskStatusEnum;
}
