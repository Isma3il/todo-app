import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TaskRepository } from "./task-repository";
import { User } from "../auth/user.entity";
import { CreateTaskDto } from "./dto/create-task.dto";
import { Task } from "./task.entity";
import { TaskFilterDto } from "./dto/task-filter.dto";
import { UpdateTaskDto } from "./dto/update-task.dto";
import { GeneralResponse } from "../../config/general-response";
import { PageTaskDto } from "./dto/page-task.dto";

@Injectable()
export class TasksService {
  constructor(@InjectRepository(TaskRepository) private readonly taskRepository: TaskRepository) {
  }

  async createTask(user: User, createTaskDto: CreateTaskDto) {
    return await this.taskRepository.createTask(user, createTaskDto);
  }

  async getTasks(user: User, pageTaskDto: PageTaskDto) {
    const { page, limit } = pageTaskDto;
    const [data, total] = await this.taskRepository.findAndCount({
      where: { user },
      take: limit||5,
      skip: limit * ((page||1) - 1)
    });
    const totalPages = (total / (limit||5)).toFixed();
    return {
      page:(+page||1),
      limit:(+limit||5),
      totalResult: total,
      totalPages:+totalPages,
      data
    };


  }

  async getTask(user: User, id: Number) {
    const task = await this.taskRepository.findOne({ where: { task_id: id, userId: user.id } });
    if (!task) {
      throw new NotFoundException("task not found");
    }
    return task;
  }

  async searchTask(user: User, taskFilterDto: TaskFilterDto): Promise<Task[]> {
    return await this.taskRepository.searchTask(user, taskFilterDto);
  }

  async updateTask(user: User, updateTaskDto: UpdateTaskDto) {
    const { id, title, description, status } = updateTaskDto;
    const generalResponse = new GeneralResponse();
    const task = await this.getTask(user, id);
    if (task) {
      task.title = title;
      task.description = description;
      task.status = status;
      await task.save();
      delete task.user;
      delete task.userId;

      generalResponse.key = 1;
      generalResponse.message = "update success";
      generalResponse.data = task;
    } else {
      generalResponse.key = 1;
      generalResponse.message = "task not found";
      delete generalResponse.data;
    }
    return generalResponse;
  }

  async deleteTask(user: User, id: number) {

    const generalResponse = new GeneralResponse();
    const result = await this.taskRepository.delete({ task_id: id, user: user });


    if (result.affected === 0) {
      generalResponse.key = 0;
      generalResponse.message = `No task with this id = ${id} `;
    } else {
      generalResponse.key = 1;
      generalResponse.message = "task deleted successfully";
    }

    delete generalResponse.data;

    return generalResponse;
  }

}
