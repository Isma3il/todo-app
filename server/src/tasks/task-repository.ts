import { EntityRepository, Repository } from "typeorm";
import { Task } from "./task.entity";
import { User } from "../auth/user.entity";
import { CreateTaskDto } from "./dto/create-task.dto";
import { InternalServerErrorException } from "@nestjs/common";
import { TaskFilterDto } from "./dto/task-filter.dto";
import { GeneralResponse } from "../../config/general-response";


@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {


  async createTask(user: User, createTaskDto: CreateTaskDto) {
    const { title, description, status } = createTaskDto;
    const generalResponse = new GeneralResponse();

    const task = new Task();
    task.title = title;
    task.description = description;
    task.status = status;
    task.user = user;

    try {
      await task.save();
      delete task.user;
      delete task.userId;
      generalResponse.key=1;
      generalResponse.message="task created successfully";
      generalResponse.data=task;


    } catch (e) {
      generalResponse.key=0;
      generalResponse.message=e;

      throw new InternalServerErrorException(e);

    }

    return generalResponse;
  }

  async searchTask(user:User,taskFilterDto:TaskFilterDto){
    const {search,status}=taskFilterDto;
    const query=this.createQueryBuilder('task');



    query.where('task.userId = :userId',{userId:user.id});

    if (status){
      query.andWhere('task.status = :status',{status});
    }

    if (search){
      query.andWhere('(task.title LIKE :search OR task.description LIKE :search)',{search:`%${search}%`})
    }


    return await query.getMany();
  }




}
