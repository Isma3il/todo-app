import {
  Body,
  Controller,
  Get, Logger, Param,
  ParseIntPipe,
  Post, Res,
  UploadedFile, UploadedFiles,
  UseGuards,
  UseInterceptors,
  ValidationPipe
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { TasksService } from "./tasks.service";
import { GetUser } from "../auth/decorator/user.decorate";
import { User } from "../auth/user.entity";
import { diskStorage } from "multer";
import { CreateTaskDto } from "./dto/create-task.dto";
import { TaskFilterDto } from "./dto/task-filter.dto";
import { UpdateTaskDto } from "./dto/update-task.dto";
import { PageTaskDto } from "./dto/page-task.dto";
import { AnyFilesInterceptor, FileInterceptor, FilesInterceptor } from "@nestjs/platform-express";
import { editFileName, imageFileFilter } from "../../utils/file-upload.utils";
import { molterConfig } from "../../config/molterConfig";

const logger = new Logger();


@Controller("tasks")
@UseGuards(AuthGuard())
export class TasksController {

  constructor(private readonly tasksService: TasksService) {
  }


  @Post("/create")
  async createTask(@GetUser() user: User, @Body(ValidationPipe) createTaskDto: CreateTaskDto) {
    return await this.tasksService.createTask(user, createTaskDto);
  }

  @Post("/get-tasks")
  async getTasks(@GetUser() user: User, @Body(ValidationPipe) pageTaskDto: PageTaskDto) {
    return await this.tasksService.getTasks(user, pageTaskDto);
  }

  @Post("/search-tasks")
  async searchTasks(@GetUser() user: User, @Body(ValidationPipe) taskFilterDto: TaskFilterDto) {
    return await this.tasksService.searchTask(user, taskFilterDto);
  }

  @Post("/update-task")
  async updateTask(@GetUser() user: User, @Body(ValidationPipe)updateTaskDto: UpdateTaskDto) {
    return await this.tasksService.updateTask(user, updateTaskDto);
  }

  @Post("/delete-task")
  async deleteTask(@GetUser() user: User, @Body("id", ParseIntPipe) id: number) {
    return await this.tasksService.deleteTask(user, id);
  }


  @Post("upload")
  @UseInterceptors(FileInterceptor("image",molterConfig))
  async uploadFile(@UploadedFile() file) {
    const response = {
      originalName: file.originalname,
      filename: file.filename
    };
    return response;
  }



}
