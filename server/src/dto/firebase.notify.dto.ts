import { IsJSON, IsNotEmpty } from "class-validator";
import { Optional } from "@nestjs/common";

export class FirebaseNotifyDto {

  @IsNotEmpty()
  fcmToken: string;
  @Optional()
  title: string='test';
  @Optional()
  body: string='test body';
  @Optional()
  data:string;

}
