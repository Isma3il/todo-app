import { IsEmail, IsNotEmpty } from "class-validator";

export class ActivateDto {
  @IsEmail()
  email:string;
  @IsNotEmpty()
  code:string;
}
