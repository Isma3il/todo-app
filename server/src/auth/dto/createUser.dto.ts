import { IsEmail, IsEnum, IsLatitude, IsLongitude, IsString, MinLength } from "class-validator";
import { Optional } from "@nestjs/common";
import { LangEnum } from "../lang-enum";

export class CreateUserDto {
  @IsString()
  @MinLength(4)
  name:string;

  @IsEmail()
  email:string;

  @Optional()
  lat:number;

  @Optional()
  lng:number;

  @IsString()
  @MinLength(8)
  password:string;

  @IsEnum(LangEnum)
  lang:LangEnum;

}
