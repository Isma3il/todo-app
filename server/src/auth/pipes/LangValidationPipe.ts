import { ArgumentMetadata, BadRequestException, PipeTransform } from "@nestjs/common";
import { LangEnum } from "../lang-enum";

export class LangValidationPipe implements PipeTransform{
  readonly allowedLang=[
    LangEnum.EN,
    LangEnum.AR
  ];

  transform(value: any, metadata: ArgumentMetadata): any {
    if (!this.isLangValid(value)){
      throw new BadRequestException(`${value} is invalid lang`);
    }
    return value;
  }

  private isLangValid(status: any) {
    const idx = this.allowedLang.indexOf(status.toUpperCase());
    return idx !== -1;
  }

}
