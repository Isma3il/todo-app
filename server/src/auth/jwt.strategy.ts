import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt';
import { InjectRepository } from "@nestjs/typeorm";
import { UserRepository } from "./user.repository";
import { JwtPayload } from "./jwt-payload.interface";
import { Injectable, UnauthorizedException } from "@nestjs/common";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
  constructor(@InjectRepository(UserRepository) private readonly userRepository:UserRepository) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration:false,
      secretOrKey: '123secret123',
    });
  }

  async validate(payload:JwtPayload){
    const{id}=payload;
    const user= await this.userRepository.findOne({id});

    if (!user){
      throw new UnauthorizedException();
    }

    return user;
  }
}
