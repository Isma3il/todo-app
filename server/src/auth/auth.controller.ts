import {
  Body,
  Controller,
  Logger,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  ValidationPipe
} from "@nestjs/common";
import { AuthService } from "./auth.service";
import { CreateUserDto } from "./dto/createUser.dto";
import { LoginUserDto } from "./dto/loginUser.dto";
import { FileInterceptor } from "@nestjs/platform-express";
import { molterConfig } from "../../config/molterConfig";
import { ActivateDto } from "./dto/activate.dto";
import { SendCodeDto } from "./dto/sendCode.dto";


let logger = new Logger();


@Controller('auth')
export class AuthController {


  constructor(private readonly authService:AuthService) {
  }


  @Post('/register')
  @UseInterceptors(FileInterceptor("avatar",molterConfig))
  async signUp(@Body(ValidationPipe) createUserDto:CreateUserDto,@UploadedFile() file){
    return this.authService.signUp(createUserDto,file);
  }

  @Post('/login')
  async login(@Body(ValidationPipe) loginUserDto:LoginUserDto){
    return this.authService.login(loginUserDto);
  }

  @Post('activate_account')
  async activateAccount(@Body(ValidationPipe)activateDto:ActivateDto){
    return this.authService.activateAccount(activateDto.email, activateDto.code);
  }

  @Post('send_activation_code')
  async sendActivationCode(@Body(ValidationPipe)sendCodeDto:SendCodeDto){
    return this.authService.sendActivationCode(sendCodeDto.email);
  }



}
