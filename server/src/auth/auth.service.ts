import { Injectable, Logger, UnauthorizedException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserRepository } from "./user.repository";
import { JwtService } from "@nestjs/jwt";
import { CreateUserDto } from "./dto/createUser.dto";
import { LoginUserDto } from "./dto/loginUser.dto";
import { JwtPayload } from "./jwt-payload.interface";
import { SERVER_URL } from "../../config/keys";
import {unlinkSync} from 'fs';


const logger =  new Logger();

@Injectable()
export class AuthService {
  constructor(@InjectRepository(UserRepository) private readonly userRepository:UserRepository,
              private readonly jwtService:JwtService) {

  }


  async signUp(createUserDto:CreateUserDto,file){
    const avatar=SERVER_URL+file.filename;
    const result= await this.userRepository.SignUp(createUserDto,avatar);

    if (result.key === 0){
      await unlinkSync("./uploads/"+file.filename);
      return result;
    }else{
      return result;
    }

  }

  async login(loginUserDto:LoginUserDto){
    const response=await this.userRepository.validatePassword(loginUserDto);

   /* if (!user){
      throw new UnauthorizedException('Invalid credentials');
    }*/

    if (response.key==1){
      const user=response.data;
      const payload:JwtPayload={
        id:user.id,
        lang:user.lang,
        notify:user.notify
      };
      const accessToken=await  this.jwtService.sign(payload);


      return {
        ...response,
        token:accessToken
      };
    }else{
      return response
    }


  }


  async activateAccount(email:string,code:string){
    return this.userRepository.activateUser(email, code);
  }

  async sendActivationCode(email:string){
    return this.userRepository.sendActivationCode(email);
  }

}
