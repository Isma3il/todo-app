import { LangEnum } from "./lang-enum";

export interface JwtPayload{
    id:number;
    lang:LangEnum;
    notify:boolean;
}
