import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import * as bcrypt from "bcrypt";
import { LangEnum } from "./lang-enum";
import { Task } from "../tasks/task.entity";

@Entity()
@Unique(['email'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  email: string;

  @Column()
  activated: boolean = false;
  @Column()
  activationCode: string = "";

  @Column('numeric')
  lat: number = 0;

  @Column('numeric')
  lng: number = 0;


  @Column()
  password: string;
  @Column()
  salt: string;
  @Column()
  lang: LangEnum = LangEnum.EN;
  @Column()
  notify: boolean = true;
  @Column()
  avatar: string = "";

  @OneToMany(type => Task, task => task.user, { eager: true })
  tasks: Task[];

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }

}
