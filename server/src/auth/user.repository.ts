import { EntityRepository, Repository } from "typeorm";
import { User } from "./user.entity";
import { CreateUserDto } from "./dto/createUser.dto";
import * as bcrypt from "bcrypt";
import * as random from "randomatic";
import { Logger } from "@nestjs/common";
import { LoginUserDto } from "./dto/loginUser.dto";
import { GeneralResponse } from "../../config/general-response";
import { sendActivationCodeToEmail } from "../../config/email.config";

let logger = new Logger();

@EntityRepository(User)
export class UserRepository extends Repository<User> {


  async SignUp(createUserDto: CreateUserDto, avatar: string) {

    const { name, email, password, lat, lng, lang } = createUserDto;
    const generalResponse = new GeneralResponse();

    const user = new User();
    user.name = name;
    user.email = email;
    user.lat = lat;
    user.lng = lng;
    user.lang = lang;
    user.salt = await bcrypt.genSalt();
    user.password = await UserRepository.hashPassword(password, user.salt);
    user.activationCode = random("0", 10);
    user.avatar = avatar;
    try {
      await user.save();
      delete user.password;
      delete user.salt;
      delete user.activationCode;
      delete user.tasks;
      sendActivationCodeToEmail(user.email, user.activationCode);
      generalResponse.key = 1;
      generalResponse.message = "User created successfully , check your email to activate it";
      generalResponse.data = user;


    } catch (err) {
      if (err.code === "23505") {
        generalResponse.key = 0;
        generalResponse.message = "User already exists";

        //throw new ConflictException("User already exists");
      } else {
        logger.error(err);
        generalResponse.key = 0;
        generalResponse.message = err;
        //throw new InternalServerErrorException();
      }
    }

    return generalResponse;


  }


  async activateUser(email: string, code: string) {
    const user = await this.findOne({ email });
    const generalResponse = new GeneralResponse<User>();
    console.log(user);
    if (user) {
      console.log(user);
      if (user.activationCode === code) {
        user.activated = true;
        user.activationCode = "";

        await user.save();

        delete user.password;
        delete user.salt;
        delete user.activationCode;
        delete user.tasks;

        generalResponse.key = 1;
        generalResponse.message = "User activated successfully";
        generalResponse.data = user;
      } else {
        generalResponse.key = 0;
        generalResponse.message = "error on activation code";
        generalResponse.data = null;
      }
    } else {
      generalResponse.key = 0;
      generalResponse.message = "user not found";
      generalResponse.data = null;
    }


    return generalResponse;
  }

  async sendActivationCode(email: string) {
    const generalResponse = new GeneralResponse<User>();
    const user = await this.findOne({ email });
    if (user) {
      user.activationCode = random("0", 10);
      await user.save();
      const sendMsg = await sendActivationCodeToEmail(user.email, user.activationCode);
      generalResponse.key = 1;
      generalResponse.message = "code sent successfully";
    } else {
      generalResponse.key = 0;
      generalResponse.message = "this email is not registered";
    }

    return generalResponse;

  }


  async validatePassword(loginUserDto: LoginUserDto) {
    const { email, password } = loginUserDto;
    const user = await this.findOne({ email });
    const generalResponse = new GeneralResponse<User>();
    console.log(user);
    if (user && await user.validatePassword(password)) {

      delete user.salt;
      delete user.activationCode;
      delete user.password;
      delete user.tasks;

      if (user.activated) {
        generalResponse.key = 1;
        generalResponse.message = "login success";
        generalResponse.data = user;
      } else {

        generalResponse.key = 0;
        generalResponse.message = "you should active user to could login";
        generalResponse.data = null;
      }

    } else {
      generalResponse.key = 0;
      generalResponse.message = "credential error";
      generalResponse.data = null;
    }

    return generalResponse;
  }


  private static async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

}
