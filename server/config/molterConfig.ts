import { MulterOptions } from "@nestjs/platform-express/multer/interfaces/multer-options.interface";
import { diskStorage } from "multer";
import { editFileName, imageFileFilter } from "../utils/file-upload.utils";

export const molterConfig:MulterOptions={
  dest:'./uploads',
  storage:diskStorage({
    destination: "./uploads",
    filename: editFileName
  }),
  fileFilter: imageFileFilter,

};
