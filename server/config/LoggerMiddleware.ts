import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
/*    console.log('request'+req.body);
    console.log('-----------------------------------');
    console.log('response...'+res.json);*/
    next();
  }
}
