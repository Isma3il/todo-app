
export class GeneralResponse<T> {
  key:number;
  message:string;
  data:T;
}
