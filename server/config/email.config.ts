import * as nodemailer from "nodemailer";
import * as sgMail from "@sendgrid/mail";
import { InternalServerErrorException } from "@nestjs/common";

/*const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "your-mail@gmail.com",
    pass: "your password"
  }
});*/

sgMail.setApiKey("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");


export function sendMail() {
  /*transporter.sendMail(mailOptions, (err, info) => {
    if(err){
      return err;
    }
    else{
      return info;
    }
  });*/
  const mailOptions = {
    from: "TodoApp@email.com", // sender address
    to: "thedevwolf@gmail.com", // list of receivers
    subject: "Todo NestJs Mailer", // Subject line
    html: "<p>Hey from nest js</p>"// plain text body
  };


  sgMail.send(mailOptions).then(res => {
    return res;
  }).catch(err => {
    return err;
  });
}

export function sendActivationCodeToEmail(email: string, code: string) {

  const mailOptions = {
    from: "TodoApp@noreply.com", // sender address
    to: email, // list of receivers
    subject: "Todo Verification Code", // Subject line
    html: `<p>Use this code to activate your account <b>${code}</b> </p>`// plain text body
  };
  sgMail.send(mailOptions).then(res => {
    return true;
    }).catch(err => {
    return false;
  });

}
