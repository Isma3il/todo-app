import * as firebase from "firebase-admin";
import * as serviceAccount from "../config/flutter-apps-f6c2b-firebase-adminsdk-hu3d1-9f97f4bafa.json";

//initial parameter attribute
const params = {
  type: serviceAccount.type,
  projectId: serviceAccount.project_id,
  privateKeyId: serviceAccount.private_key_id,
  privateKey: serviceAccount.private_key,
  clientEmail: serviceAccount.client_email,
  clientId: serviceAccount.client_id,
  authUri: serviceAccount.auth_uri,
  tokenUri: serviceAccount.token_uri,
  authProviderX509CertUrl: serviceAccount.auth_provider_x509_cert_url,
  clientC509CertUrl: serviceAccount.client_x509_cert_url
};

//initial admin
const admin = firebase.initializeApp({
  credential: firebase.credential.cert(params)
});

const options = {
  priority: "high",
  timeToLive: 60 * 60 * 24
};

export function Pusher(token: string, title:string,msg:string,data:string) {


  const payload = {
    data: {
      title,
      msg,
      data:data.replace('\\','')
    }
  };


  admin.messaging().sendToDevice(token, payload, options).then(response => {
    console.log(`Message Sent successfully ${response}`);
    return 'Message Sent successfully';
  }).catch(err => {
    console.log(`Message Failed to send ${err}`);
    return 'Message Failed to send';
  });


}
